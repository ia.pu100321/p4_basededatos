package com.sv.edu.ufg.fis.amb.consultabasededatos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sv.edu.ufg.fis.amb.consultabasededatos.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.botonMain.setOnClickListener{
            val intent = Intent(this,Guardar::class.java)
            startActivity(intent)
        }
    }
}